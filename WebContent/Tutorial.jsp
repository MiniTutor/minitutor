<!DOCTYPE html>
<html>
<head>
<style>
.Language {
  background-color: tomato;
  color: white;
  border: 2px solid black;
  margin: 20px;
  padding: 20px;
}
.Language1 {
  background-color: #0080ff;
  color: white;
  border: 2px solid black;
  margin: 20px;
  padding: 20px;
}
</style>
</head>
<body>

<div class="Language">
<a href="https://www.javatpoint.com/java-tutorial"><h2>Learn Java Programming language</h2></a>
<p>Java is a class-based, object-oriented programming language that is designed to have as few implementation dependencies as possible.</p>
</div> 

<div class="Language1">
<a href="https://www.javatpoint.com/spring-boot-tutorial"><h2>Learn Spring Boot Programming language</h2></a>
<p>The Spring Framework is an application framework and inversion of control container for the Java platform.</p>
</div>

<div class="Language">
<a href="https://www.javatpoint.com/hibernate-tutorial"><h2>Learn Hibernate Programming language</h2></a>
<p>Hibernate ORM is an object–relational mapping tool for the Java programming language.</p>
</div>

<div class="Language1">
<a href="https://www.javatpoint.com/java-mail-api-tutorial"><h2>Learn Java Mail API</h2></a>
<p>The JavaMail is an API that is used to compose, write and read electronic messages (emails).</p>
</div>

</body>
</html>
